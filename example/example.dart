import 'package:dart_monkey/dart_monkey.dart';

/// Sample solely for debugging purposes while developing the Dart Monkey
void main() async {
  final server = MonkeyServer();

  final controller = SampleController();
  server.route("/endpoint", controller);

  await server.start(
    address: 'localhost',
    port: 8080,
    hotReload: true,
  );
}

class SampleController extends Controller {
  @override
  Future<ApiResponse> create(
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) {
    throw UnimplementedError();
  }

  @override
  Future<ApiResponse> delete(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) {
    throw UnimplementedError();
  }

  @override
  Future<ApiResponse> read(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) {
    throw UnimplementedError();
  }

  @override
  Future<ApiResponse> readAll(
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) async {
    return ApiResponse(statusCode: 200, body: {'response': 'readAll called'});
  }

  @override
  Future<ApiResponse> update(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) {
    throw UnimplementedError();
  }
}
