import 'package:dart_monkey/src/database_interface.dart';
import 'package:dart_monkey/src/exceptions.dart';
import 'package:dart_monkey/src/migration.dart';

class DatabaseMigrationExecutor {
  final List<DatabaseMigration> migrations;
  final IDatabase database;

  DatabaseMigrationExecutor(this.migrations, this.database);

  Future<void> up() async {
    await _verifyConsistency();

    // Get active migrations in the database
    final migrationTitlesInDB = await database.allMigrationTitles();

    // Apply the missing migrations in the passed order
    for (final migration in migrations) {
      if (!migrationTitlesInDB.contains(migration.uniqueTitle)) {
        await database.applyMigration(migration);
      }
    }
  }

  Future<void> down() async {
    await _verifyConsistency();

    // Get active migrations in the database
    final migrationTitlesInDB = await database.allMigrationTitles();

    // Fail if there are no migrations anymore to remove
    if (migrationTitlesInDB.isEmpty) {
      throw DatabaseMigrationException(
        reason: 'No migrations available to remove',
      );
    }

    // Remove the latest one
    final indexToRemove = migrationTitlesInDB.length - 1;
    await database.revertMigration(migrations[indexToRemove]);
  }

  Future<void> _verifyConsistency() async {
    final migrationTitlesInDB = await database.allMigrationTitles();
    final countInDatabase = migrationTitlesInDB.length;
    final countInApp = migrations.length;

    // Verify that the migrations contain no duplicates
    final uniqueMigrations = migrations.map((e) => e.uniqueTitle).toSet();
    if (migrations.length != uniqueMigrations.length) {
      throw DatabaseMigrationException(
        reason: 'The provided list of migrations contains duplicates',
      );
    }

    // Verify that our database does not have more migrations than we know about
    if (countInDatabase > countInApp) {
      throw DatabaseMigrationException(
        reason:
            'Database has unknown migrations. Database has $countInDatabase, app only knows $countInApp',
      );
    }

    // Verify that the migrations in the database are consistent, i.e.
    // that there are no gaps in between
    for (int i = 0; i < countInDatabase; i++) {
      if (migrationTitlesInDB[i] != migrations[i].uniqueTitle) {
        throw DatabaseMigrationException(
          reason:
              'Unexpected migration in database. Expected ${migrations[i].uniqueTitle} but found ${migrationTitlesInDB[i]}',
        );
      }
    }
  }
}
