import 'package:dart_monkey/src/reflector.dart';
import 'package:dart_monkey/src/types.dart';
import 'package:recase/recase.dart';
import 'package:reflectable/reflectable.dart';

/// Note: Member = member in class (typically CamelCase), field = column in database (snake_case)
///
/// Mixin which provides access to class variables
///
/// [Reflectable] extends the class with methods for dynamically getting and
/// setting variables as 'fields'. Note that field names' are in snake_case.
/// [Reflectable] automatically returns then in snake_case in getter methods
/// such as [reflectableFields] and expects then in snake_case in setter methods
/// such as [setFieldValue].
mixin Reflectable {
  FieldValueMap reflectedFields() {
    // Get all members of the class
    final classMirror = monkeyReflector.reflectType(runtimeType) as ClassMirror;
    final Map<String, MethodMirror> instanceMembers =
        classMirror.instanceMembers;

    // Find all explicitely defined fields in these members
    final FieldValueMap resultMap = {};
    for (final member in instanceMembers.values) {
      if (member.isGetter && member.isSynthetic && !member.isPrivate) {
        // Get the value of that field on this instance
        final memberName = member.simpleName;
        final fieldName = _memberToFieldName(memberName);
        final value = monkeyReflector.reflect(this).invokeGetter(memberName);
        resultMap[fieldName] = value;
      }
    }

    return resultMap;
  }

  void setFieldValue(String fieldName, dynamic value) {
    final memberName = _fieldToMemberName(fieldName);
    if (!hasField(fieldName)) {
      throw Exception(
        "Field named '$fieldName' (member: $memberName) does not exist in object",
      );
    }
    final instanceMirror = monkeyReflector.reflect(this);
    instanceMirror.invokeSetter(memberName, value);
  }

  /// Sets the values for the passed [fields]
  /// Throws an exception if a passed field does not exist in this object
  void setFieldValues(FieldValueMap fields) {
    fields.forEach((key, value) {
      setFieldValue(key, value);
    });
  }

  dynamic fieldValue(String fieldName) {
    return reflectedFields()[fieldName];
  }

  bool hasField(String name) {
    return reflectedFields().containsKey(name);
  }

  /// Gets the class name in snake_case and plural
  String tableName() {
    return tableNameForClass(runtimeType);
  }

  /// Gets the class name for class [type] in snake_case and plural
  static String tableNameForClass(Type type) {
    final classMirror = monkeyReflector.reflectType(type) as ClassMirror;
    final className = _memberToFieldName(classMirror.simpleName);
    return "${className}s";
  }

  static T createInstance<T>() {
    final classMirror = monkeyReflector.reflectType(T) as ClassMirror;
    return classMirror.newInstance('', []) as T;
  }

  /// Converts [fieldName] from camelCase to under_lines
  static String _memberToFieldName(String fieldName) {
    return ReCase(fieldName).snakeCase;
  }

  /// Converts [rowName] from under_lines to camelCase
  static String _fieldToMemberName(String rowName) {
    return ReCase(rowName).camelCase;
  }
}
