// Annotate with this class to enable reflection.
import 'package:reflectable/reflectable.dart';

class MonkeyReflector extends Reflectable {
  const MonkeyReflector()
      : super(
          invokingCapability,
          declarationsCapability,
          newInstanceCapability,
          typeCapability,
        );
}

const monkeyReflector = MonkeyReflector();
