class ValidationException implements Exception {
  String attrName;
  String cause;

  ValidationException({required this.attrName, required this.cause});

  @override
  String toString() {
    return "Attribute '$attrName' $cause";
  }
}

class ResourceNotFoundException implements Exception {
  String tableName;

  ResourceNotFoundException({required this.tableName});

  @override
  String toString() {
    return "Requested resource not found in '$tableName'";
  }
}

class DatabaseMigrationException implements Exception {
  String reason;

  DatabaseMigrationException({required this.reason});

  @override
  String toString() {
    return "Database migration failed. Reason: $reason";
  }
}
