import 'package:dart_monkey/src/database_dev.dart';
import 'package:dart_monkey/src/database_interface.dart';

class Environment {
  final IDatabase database;

  Environment({required this.database});

  static void reset() {
    environment = Environment(database: DevelopmentDatabase());
  }
}

Environment environment = Environment(database: DevelopmentDatabase());
