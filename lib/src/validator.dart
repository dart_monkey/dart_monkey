import 'package:dart_monkey/src/exceptions.dart';
import 'package:dart_monkey/src/types.dart';

abstract class IValidator {
  void validate(JsonMap requestBody);
}

class GroupValidator implements IValidator {
  List<IValidator> validators;

  GroupValidator(this.validators);

  @override
  void validate(JsonMap bodyAsJson) {
    for (final validator in validators) {
      validator.validate(bodyAsJson);
    }
  }
}

/// Base class for validators who validate a member specified via path
abstract class PathValidatorBase implements IValidator {
  String path;

  PathValidatorBase({required this.path}) {
    // If a single member name is passed, treat it as attribute
    if (!path.contains('.')) {
      path = "data.attributes.$path";
    }
  }

  @override
  void validate(JsonMap requestBody) {
    final value = extractValue(requestBody);
    doValidate(value);
  }

  /// Finds the member value for [path] in [requestBody]
  dynamic extractValue(JsonMap requestBody) {
    dynamic currentMember = requestBody;

    try {
      final memberNameList = path.split(".");
      for (final memberName in memberNameList) {
        currentMember = (currentMember as JsonMap)[memberName];
      }
      return currentMember;
    } catch (e) {
      return null;
    }
  }

  void doValidate(dynamic value);
}

/// Notes: treats single names as attributes ('attr' => 'data.attributes.attr')
class StringValidator extends PathValidatorBase {
  bool? required;

  StringValidator({
    required super.path,
    this.required,
  });

  @override
  void doValidate(dynamic value) {
    if (required == true && (value == null || value.toString().isEmpty)) {
      throw ValidationException(attrName: path, cause: 'is required');
    }
  }
}
