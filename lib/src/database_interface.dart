import 'package:dart_monkey/src/database_connection.dart';
import 'package:dart_monkey/src/migration.dart';
import 'package:dart_monkey/src/types.dart';

abstract class IDatabase {
  Future<void> connect(DatabaseSettings settings);
  Future<void> close();

  /// Note: Fills 'id' in the row
  Future<int> createRow(String tableName, DatabaseRow fields);

  Future<void> updateRow(
    String tableName,
    int id,
    DatabaseRow updatedFields,
  );

  Future<DatabaseRow> readRow(String tableName, int id);

  Future<void> deleteRow(String tableName, int id);

  Future<DatabaseRow?> findRowByField(
    String tableName,
    String fieldName,
    dynamic value,
  );

  Future<void> applyMigration(DatabaseMigration migration);
  Future<void> revertMigration(DatabaseMigration migration);
  Future<List<String>> allMigrationTitles();
}
