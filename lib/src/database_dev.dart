import 'package:collection/collection.dart';
import 'package:dart_monkey/src/database_connection.dart';
import 'package:dart_monkey/src/database_interface.dart';
import 'package:dart_monkey/src/migration.dart';
import 'package:dart_monkey/src/types.dart';

class DevelopmentDatabase implements IDatabase {
  final Map<String, List<DatabaseRow>> _data = {};

  int lastChangedFieldsCount = 0;

  List<DatabaseRow> rows(String tableName) {
    return _data[tableName] ?? [];
  }

  DatabaseRow? rowById(String tableName, int id) {
    final rowList = rows(tableName);
    return rowList.firstWhereOrNull((e) => e['id'] == id);
  }

  @override
  Future<void> connect(DatabaseSettings settings) async {}

  @override
  Future<void> close() async {}

  @override
  Future<int> createRow(String tableName, DatabaseRow fields) async {
    if (!_data.containsKey(tableName)) {
      _data[tableName] = [];
    }

    fields['id'] = rows(tableName).length;
    _data[tableName]!.add(fields);
    lastChangedFieldsCount = fields.length;

    return fields['id'] as int;
  }

  @override
  Future<void> deleteRow(String tableName, int id) async {
    if (_data.containsKey(tableName)) {
      _data[tableName]!.removeWhere((row) => row['id'] == id);
    }
  }

  @override
  Future<DatabaseRow> readRow(String tableName, int id) {
    final result = _data[tableName]!.firstWhere((row) => row['id'] == id);
    return Future.value(result);
  }

  @override
  Future<void> updateRow(
    String tableName,
    int id,
    DatabaseRow updatedFields,
  ) async {
    lastChangedFieldsCount = updatedFields.length;

    final row = await readRow(tableName, id);
    final index = _data[tableName]!.indexOf(row);

    updatedFields.forEach((key, value) => row[key] = value);
    _data[tableName]![index] = row;
  }

  @override
  Future<DatabaseRow?> findRowByField(
    String tableName,
    String fieldName,
    dynamic value,
  ) {
    DatabaseRow? result;
    if (_data.containsKey(tableName)) {
      result = _data[tableName]!.firstWhereOrNull(
        (row) => row[fieldName] == value,
      );
    }

    return Future.value(result);
  }

  @override
  Future<void> applyMigration(DatabaseMigration migration) async {
    // Nothing to do
  }

  @override
  Future<void> revertMigration(DatabaseMigration migration) async {
    // Nothing to do
  }

  @override
  Future<List<String>> allMigrationTitles() async {
    return [];
  }
}
