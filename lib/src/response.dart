import 'dart:convert';

import 'package:dart_monkey/src/resource.dart';
import 'package:dart_monkey/src/types.dart';
import 'package:shelf/shelf.dart' as shelf;

class ApiResponse {
  final shelf.Response _response;
  static const contentTypeHeader = {'content-type': 'application/vnd.api+json'};

  ApiResponse({
    required int statusCode,
    JsonMap? body,
    Map<String, Object>? headers,
    Encoding? encoding,
  }) : this._(
          shelf.Response(
            statusCode,
            body: body != null ? jsonEncode(body) : null,
            headers: headers,
            encoding: encoding,
          ),
        );

  ApiResponse._(this._response);

  shelf.Response get shelfResponse => _response;

  Future<String> body() => _response.readAsString();

  Future<JsonMap> json() async => jsonDecode(await body()) as JsonMap;

  int get statusCode => _response.statusCode;

  factory ApiResponse.resource({
    required int statusCode,
    required Resource resource,
    required shelf.Request request,
  }) {
    if (statusCode < 200 || statusCode >= 300) {
      throw Exception("Invalid status code '$statusCode' for error response");
    }

    final body = {
      "links": {
        "self": request.requestedUri.toString(),
      },
      "data": {
        "type": resource.tableName(),
        "id": resource.searchId(),
        "attributes": {
          for (var attr in resource.attributes.entries)
            attr.key: attr.value.toString(),
        }
      }
    };

    return ApiResponse(
      statusCode: statusCode,
      body: body,
      headers: contentTypeHeader,
    );
  }

  factory ApiResponse.error({
    required int statusCode,
    required String title,
    String? detail,
  }) {
    if (statusCode < 400 || statusCode >= 600) {
      throw Exception("Invalid status code '$statusCode' for error response");
    }
    final body = {
      'errors': {
        'status': statusCode.toString(),
        'title': title,
        if (detail != null) 'detail': detail,
      }
    };

    return ApiResponse(
      statusCode: statusCode,
      body: body,
      headers: contentTypeHeader,
    );
  }
}
