typedef FieldValueMap = Map<String, dynamic>;
typedef JsonMap = Map<String, dynamic>;
typedef DatabaseRow = Map<String, dynamic>;
typedef AttributeMap = Map<String, dynamic>;
