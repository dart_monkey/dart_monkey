import 'package:dart_monkey/dart_monkey.dart';
import 'package:dart_monkey/src/environment.dart';
import 'package:dart_monkey/src/exceptions.dart';
import 'package:dart_monkey/src/reflectable.dart';

// ignore: avoid_classes_with_only_static_members
class Search {
  static Future<T?> where<T>(String fieldName, dynamic value) async {
    T? instance;

    final tableName = Reflectable.tableNameForClass(T);
    final row = await environment.database.findRowByField(
      tableName,
      fieldName,
      value,
    );

    if (row != null) {
      instance = Reflectable.createInstance<T>();

      final resourceInstance = instance! as Resource;
      resourceInstance.setFieldValues(row);
    }

    return instance;
  }

  static Future<T> whereOrFail<T>(String fieldName, dynamic value) async {
    final result = await where<T>(fieldName, value);
    if (result != null) {
      return result;
    } else {
      final tableName = Reflectable.tableNameForClass(T);
      throw ResourceNotFoundException(tableName: tableName);
    }
  }
}
