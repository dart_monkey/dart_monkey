import 'package:dart_monkey/src/reflector.dart';
import 'package:dart_monkey/src/resource.dart';
import 'package:dart_monkey/src/types.dart';

/// Resource base class for typical database resources
///
/// This resource provides the fields [id], [createdAt] and [updatedAt]. Derive
/// your own resources from this class if the following this standard scheme.
@monkeyReflector
class DatabaseResource extends Resource {
  int? id;
  DateTime? createdAt;
  DateTime? updatedAt;

  @override
  String get idFieldName => "id";

  @override
  String get searchIdFieldName => "id";

  @override
  Future<void> save() async {
    if (id == null) {
      // Set createdAt and updatedAt on creation
      createdAt = DateTime.now();
      updatedAt = DateTime.now();
    } else {
      // Do not allow updates if 'id' or 'createdAt' changed
      if (id != fieldsAfterLastSave[idFieldName]) {
        throw Exception("Failed on saving resource. Reason: 'id' changed");
      }
      if (createdAt != fieldsAfterLastSave['created_at']) {
        throw Exception(
          "Failed on saving resource. Reason: 'createdAt' changed",
        );
      }
      // Update the updatedAt field
      updatedAt = DateTime.now();
    }

    await super.save();
  }

  @override
  FieldValueMap get editableAttributes {
    // Filter out the createdAt member, since it is not editable
    final result = super.editableAttributes;
    result.removeWhere((key, value) => key == 'created_at');
    return result;
  }
}
