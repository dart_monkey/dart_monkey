import 'dart:developer';
import 'dart:io';

import 'package:dart_monkey/src/controller.dart';
import 'package:dart_monkey/src/database_connection.dart';
import 'package:dart_monkey/src/database_interface.dart';
import 'package:dart_monkey/src/database_sql.dart';
import 'package:dart_monkey/src/environment.dart';
import 'package:dart_monkey/src/migration.dart';
import 'package:dart_monkey/src/migration_executer.dart';
import 'package:dart_monkey/src/version.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as shelf_io;
import 'package:shelf_hotreload/shelf_hotreload.dart';
import 'package:shelf_router/shelf_router.dart';

class MonkeyServer {
  final _router = Router();
  HttpServer? _server;
  IDatabase? _database;

  Future<void> connectToDatabase({
    required DatabaseConnection conn,
    required DatabaseSettings settings,
    List<DatabaseMigration>? migrations,
  }) async {
    if (_server != null) {
      throw Exception(
        "The server is already running. Configure your database before calling start().",
      );
    }

    // Choose the database
    switch (conn) {
      case DatabaseConnection.mysql:
        _database = SqlDatabase();
        break;
    }

    // Initialize it and change the app environment to use it
    if (_database != null) {
      await _database!.connect(settings);
      environment = Environment(database: _database!);

      // Update the database to the latest schema
      if (migrations != null) {
        final executer = DatabaseMigrationExecutor(migrations, _database!);
        await executer.up();
      }
    }
  }

  /// Opens the server connection
  /// Returns immediately but keeps app alive until it is killed or stop() is called
  Future<void> start({
    required Object address,
    required int port,
    bool hotReload = false,
  }) async {
    log('Starting server at http://$address:$port');

    if (hotReload) {
      // Ignore hot-reload in coverage. We can't unit test this (causes exception)
      // coverage:ignore-start
      withHotreload(() async => _startInternal(address: address, port: port));
      // coverage:ignore-end
    } else {
      await _startInternal(address: address, port: port);
    }
  }

  Future<HttpServer> _startInternal({
    required Object address,
    required int port,
  }) async {
    _server = await shelf_io.serve(_router, address, port);
    _server!.autoCompress = true;
    log('Serving at http://${_server!.address.host}:${_server!.port}');
    return _server!;
  }

  /// Stops the server immediately
  Future<void> stop() async {
    if (_server != null) {
      await _server!.close();
      _server = null;
    }
  }

  String version() {
    return packageVersion;
  }

  void route(String route, Controller controller) {
    _router.post(route, (Request request) async {
      final response = await controller.execute(request);
      return response.shelfResponse;
    });
    _router.get(route, (Request request) async {
      final response = await controller.execute(request);
      return response.shelfResponse;
    });
    _router.get('$route/<id>', (Request request, String id) async {
      final response = await controller.execute(request, id);
      return response.shelfResponse;
    });
    _router.delete('$route/<id>', (Request request, String id) async {
      final response = await controller.execute(request, id);
      return response.shelfResponse;
    });
    _router.patch('$route/<id>', (Request request, String id) async {
      final response = await controller.execute(request, id);
      return response.shelfResponse;
    });
  }
}
