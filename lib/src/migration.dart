abstract class DatabaseMigration {
  MigrationBlueprint up();
  MigrationBlueprint down();
  String get uniqueTitle;
}

class MigrationBlueprint {
  final List<String> _queries = [];

  void createTable(String sqlQuery) {
    _queries.add(sqlQuery);
  }

  void dropTable(String tableName) {
    _queries.add('DROP TABLE $tableName');
  }

  List<String> get resultingQueries => _queries;
}

typedef DatabaseMigrationList = List<DatabaseMigration>;
