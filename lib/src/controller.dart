import 'dart:convert';

import 'package:dart_monkey/src/exceptions.dart';
import 'package:dart_monkey/src/response.dart';
import 'package:dart_monkey/src/types.dart';
import 'package:dart_monkey/src/validator.dart';
import 'package:http_status_code/http_status_code.dart';
import 'package:shelf/shelf.dart';

abstract class Controller {
  JsonMap _body = {};
  AttributeMap _attributes = {};

  Future<ApiResponse> create(
    AttributeMap attributes,
    JsonMap body,
    Request request,
  );
  Future<ApiResponse> delete(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  );
  Future<ApiResponse> read(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  );
  Future<ApiResponse> readAll(
    AttributeMap attributes,
    JsonMap body,
    Request request,
  );
  Future<ApiResponse> update(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  );

  static const _supportedMethods = ['GET', 'POST', 'PATCH', 'DELETE'];

  Future<ApiResponse> execute(Request request, [String? id]) async {
    final method = request.method;

    // Parse request body
    _body = await _readBody(request);
    _attributes = _extractAttributes(_body);

    // Delegate to the proper REST handler
    if (_supportedMethods.contains(method)) {
      try {
        if (method == "POST" && id == null) {
          return await create(_attributes, _body, request);
        } else if (method == "GET" && id == null) {
          return await readAll(_attributes, _body, request);
        } else if (method == "GET" && id != null) {
          return await read(id, _attributes, _body, request);
        } else if (method == "PATCH" && id != null) {
          return await update(id, _attributes, _body, request);
        } else if (method == "DELETE" && id != null) {
          return await delete(id, _attributes, _body, request);
        } else {
          // Fail due to a wrong combination of HTTP method and ID parameter
          return ApiResponse.error(
            statusCode: StatusCode.BAD_REQUEST,
            title: 'Invalid parameters for method',
            detail: id != null ? "No 'id' expected" : "'id' required",
          );
        }
      } on ValidationException catch (e) {
        return ApiResponse.error(
          statusCode: StatusCode.BAD_REQUEST,
          title: 'Validation error',
          detail: e.toString(),
        );
      } on ResourceNotFoundException catch (e) {
        return ApiResponse.error(
          statusCode: StatusCode.NOT_FOUND,
          title: 'Resource not found',
          detail: e.toString(),
        );
      } catch (e) {
        return ApiResponse.error(
          statusCode: StatusCode.INTERNAL_SERVER_ERROR,
          title: 'Unknown error',
          detail: e.toString(),
        );
      }
    }

    // Fail due to an unsupported HTTP method
    return ApiResponse.error(
      statusCode: StatusCode.METHOD_NOT_ALLOWED,
      title: 'Method not allowed',
      detail: "The HTTP method $method is not supported",
    );
  }

  void validate({
    required List<IValidator> validators,
  }) {
    return GroupValidator(validators).validate(_body);
  }

  Future<JsonMap> _readBody(Request request) async {
    try {
      return jsonDecode(await request.readAsString()) as Map<String, dynamic>;
    } catch (e) {
      return {};
    }
  }

  AttributeMap _extractAttributes(JsonMap body) {
    AttributeMap attributes = {};
    if (body.containsKey('data')) {
      final data = body['data'] as JsonMap;
      if (data.containsKey('attributes')) {
        attributes = data['attributes'] as AttributeMap;
      }
    }

    return attributes;
  }
}
