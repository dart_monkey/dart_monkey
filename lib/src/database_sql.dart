import 'package:dart_monkey/src/database_connection.dart';
import 'package:dart_monkey/src/database_interface.dart';
import 'package:dart_monkey/src/migration.dart';
import 'package:dart_monkey/src/types.dart';
import 'package:mysql1/mysql1.dart';

class SqlDatabase implements IDatabase {
  static const String _migrationsTableName = 'migrations';
  MySqlConnection? conn;

  @override
  Future<void> connect(DatabaseSettings settings) async {
    // First connect without database and make sure the database exists
    final initialCS = _toConnectionSettings(settings.copyWith(database: ""));
    conn = await MySqlConnection.connect(initialCS);
    await conn!.query(
      'CREATE DATABASE IF NOT EXISTS ${settings.database} CHARACTER SET utf8',
    );
    await conn!.close();

    // Now establish the connection to the database
    final connSettings = _toConnectionSettings(settings);
    conn = await MySqlConnection.connect(connSettings);
  }

  @override
  Future<void> close() async {
    if (conn != null) {
      conn!.close();
    }
  }

  @override
  Future<int> createRow(String tableName, DatabaseRow fields) async {
    final fieldsDb = _toDatabaseFormat(fields);

    final fieldListAsString = fieldsDb.keys.join(", ");
    final placeholderList = List.filled(fieldsDb.length, "?").join(", ");
    final valueList = fieldsDb.values.map((value) => value).toList();

    final sqlQuery =
        "INSERT into $tableName ($fieldListAsString) values ($placeholderList)";

    final result = await conn!.query(sqlQuery, valueList);
    return result.insertId!;
  }

  @override
  Future<void> updateRow(
    String tableName,
    int id,
    DatabaseRow updatedFields,
  ) async {
    final updatedFieldsDb = _toDatabaseFormat(updatedFields);

    final List<String> fieldList = [];
    for (final key in updatedFieldsDb.keys) {
      fieldList.add("$key=?");
    }
    final valueList = updatedFieldsDb.values.map((value) => value).toList();
    valueList.add(id);

    final fieldListAsString = fieldList.join(", ");
    final sqlQuery = "UPDATE $tableName SET $fieldListAsString WHERE id=?";

    await conn!.query(sqlQuery, valueList);
  }

  @override
  Future<DatabaseRow> readRow(String tableName, dynamic id) async {
    final sqlQuery = "SELECT * FROM $tableName WHERE id = ?";
    final results = await conn!.query(sqlQuery, [id]);

    if (results.length != 1) {
      throw Exception("Row for id $id in table $tableName not found");
    }

    final DatabaseRow result = _fromDatabaseFormat(results.first.fields);
    return result;
  }

  @override
  Future<void> deleteRow(String tableName, int id) async {
    final sqlQuery = "DELETE FROM $tableName WHERE id = ?";
    await conn!.query(sqlQuery, [id]);
  }

  @override
  Future<DatabaseRow?> findRowByField(
    String tableName,
    String fieldName,
    dynamic value,
  ) async {
    final sqlQuery = "SELECT * FROM $tableName WHERE $fieldName = ?";
    final results = await conn!.query(sqlQuery, [value]);

    if (results.isNotEmpty) {
      final DatabaseRow resultOut = {};
      results.first.fields.forEach((key, value) {
        resultOut[key] = value;
      });

      return resultOut;
    } else {
      return null;
    }
  }

  @override
  Future<void> applyMigration(DatabaseMigration migration) async {
    await _ensureMigrationTableExists();

    // Fail if the migration has been applied before
    final existingMigrations = await allMigrationTitles();
    if (existingMigrations.contains(migration.uniqueTitle)) {
      throw Exception(
        "Failed to apply migration. Reason: migration ${migration.uniqueTitle} already exists",
      );
    }

    // Execute the migration query
    final blueprint = migration.up();
    for (final query in blueprint.resultingQueries) {
      await conn!.query(query);
    }

    // Add the migration to the 'migrations' table
    await conn!.query(
      "INSERT into $_migrationsTableName (title) values (?)",
      [migration.uniqueTitle],
    );
  }

  @override
  Future<void> revertMigration(DatabaseMigration migration) async {
    await _ensureMigrationTableExists();

    // Fail if the migration has not been applied before
    final existingMigrations = await allMigrationTitles();
    if (!existingMigrations.contains(migration.uniqueTitle)) {
      throw Exception(
        "Failed to revert migration. Reason: migration ${migration.uniqueTitle} does not exist",
      );
    }

    // Execut the migration query
    final blueprint = migration.down();
    for (final query in blueprint.resultingQueries) {
      await conn!.query(query);
    }

    // Remove the migration from the 'migrations' table
    await conn!.query(
      "DELETE FROM $_migrationsTableName WHERE title = ?",
      [migration.uniqueTitle],
    );
  }

  @override
  Future<List<String>> allMigrationTitles() async {
    await _ensureMigrationTableExists();

    final results =
        await conn!.query("SELECT * FROM $_migrationsTableName ORDER BY id");
    final titles = results.map((row) => row.fields['title'] as String).toList();
    return titles;
  }

  Future<void> _ensureMigrationTableExists() async {
    const query = '''
      CREATE TABLE IF NOT EXISTS migrations (
        id INTEGER NOT NULL AUTO_INCREMENT,
        title VARCHAR(255) NOT NULL,
        PRIMARY KEY (id)
      );
    ''';

    await conn!.query(query);
  }

  DatabaseRow _toDatabaseFormat(DatabaseRow row) {
    final DatabaseRow result = {};
    row.forEach((key, value) {
      var valueDb = value;
      if (value is DateTime) {
        valueDb = value.toUtc();
      }
      result[key] = valueDb;
    });
    return result;
  }

  DatabaseRow _fromDatabaseFormat(DatabaseRow row) {
    final DatabaseRow result = {};
    row.forEach((key, value) {
      var valueApp = value;
      if (value is DateTime) {
        valueApp = value.toLocal();
      }
      result[key] = valueApp;
    });
    return result;
  }

  ConnectionSettings _toConnectionSettings(DatabaseSettings settings) {
    return ConnectionSettings(
      host: settings.host,
      port: settings.port,
      user: settings.username,
      password: settings.password,
      db: settings.database,
    );
  }
}
