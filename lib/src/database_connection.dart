enum DatabaseConnection {
  mysql,
}

class DatabaseSettings {
  final String host;
  final int port;
  final String username;
  final String password;
  final String database;

  DatabaseSettings({
    this.host = 'localhost',
    this.port = 3306,
    required this.username,
    required this.password,
    required this.database,
  });

  DatabaseSettings copyWith({
    String? host,
    int? port,
    String? username,
    String? password,
    String? database,
  }) {
    return DatabaseSettings(
      host: host ?? this.host,
      port: port ?? this.port,
      username: username ?? this.username,
      password: password ?? this.password,
      database: database ?? this.database,
    );
  }
}
