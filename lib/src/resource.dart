import 'package:dart_monkey/src/environment.dart';
import 'package:dart_monkey/src/reflectable.dart';
import 'package:dart_monkey/src/types.dart';

/// Interface for your resource classes (models) in Monkey
///
/// [Resource] uses the concept of 'fields' and 'attributes'. Fields are all
/// variables in your derived resource class. Attributes are all fields except
/// for the 'id' one. Editable fields might be limited further.
abstract class Resource with Reflectable {
  FieldValueMap _previousFieldData = {};

  // The name of the field which is used as unique identifier, in snake_case
  String get idFieldName;

  /// The name of the field returned by [searchId], in snake_case
  String get searchIdFieldName;

  Future<void> save() async {
    final db = environment.database;

    if (_resourceId() == null) {
      // Create the new row in the database and immediately fetch its data
      final id = await db.createRow(tableName(), attributes);
      final fetchedData = await db.readRow(tableName(), id);
      setFieldValues(fetchedData);
    } else {
      // Update the fields that changed
      final updatedFields = _updatedFields();
      await db.updateRow(tableName(), _resourceId()!, updatedFields);
    }

    // Remember the current field data
    _previousFieldData = reflectedFields();
  }

  Future<void> delete() async {
    final id = _resourceId();
    if (id != null) {
      return environment.database.deleteRow(tableName(), id);
    } else {
      throw Exception("Can not delete resource without id");
    }
  }

  /// Gets the value of the field which is exposed as 'id' in HTTP responses
  dynamic searchId() {
    return fieldValue(searchIdFieldName);
  }

  /// All fields this object offers
  FieldValueMap get fields {
    return reflectedFields();
  }

  /// All attributes. Attributes are all fields except the 'id' field.
  FieldValueMap get attributes {
    final attributes = reflectedFields();
    attributes.removeWhere((key, value) => key == idFieldName);
    return attributes;
  }

  /// All attributes which are editable, i.e. can still be modified after creation
  FieldValueMap get editableAttributes {
    final fields = reflectedFields();
    fields.removeWhere((key, value) => key == idFieldName);
    return fields;
  }

  /// Data of the fields at the time they were last saved
  FieldValueMap get fieldsAfterLastSave => _previousFieldData;

  FieldValueMap _updatedFields() {
    final FieldValueMap updatedFields = {};

    editableAttributes.forEach((key, value) {
      if (!_previousFieldData.containsKey(key) ||
          _previousFieldData[key] != value) {
        updatedFields[key] = value;
      }
    });

    return updatedFields;
  }

  /// Gets the value of the id property on this instance
  /// Returns null if the id property is not yet initialized
  int? _resourceId() {
    final value = fieldValue(idFieldName);
    return value != null ? value as int : null;
  }
}
