// ignore_for_file: unused_import

import 'package:dart_monkey/dart_monkey.dart';

/// Main method for the sole purpose to trigger the Reflectable build step
/// for our internal classes which have reflection enabled.
void main() async {}
