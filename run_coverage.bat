REM dart pub global activate remove_from_coverage
call flutter test --coverage
dart pub global run remove_from_coverage:remove_from_coverage -f coverage/lcov.info -r "\.reflectable\.dart$"