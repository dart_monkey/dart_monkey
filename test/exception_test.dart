import 'package:dart_monkey/src/exceptions.dart';
import 'package:test/test.dart';

void main() {
  test('Validation exception contains attribute name', () {
    final exception = ValidationException(attrName: "foo", cause: "because");
    expect(exception.toString(), "Attribute 'foo' because");
  });

  test('ResourceNotFound exception contains table name', () {
    final exception = ResourceNotFoundException(tableName: 'foo');
    expect(exception.toString(), "Requested resource not found in 'foo'");
  });

  test('DatabaseMigrationTable exception contains reason', () {
    final exception = DatabaseMigrationException(reason: 'foo');
    expect(exception.toString(), 'Database migration failed. Reason: foo');
  });
}
