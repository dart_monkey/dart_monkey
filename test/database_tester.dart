import 'dart:io';

import 'package:dart_monkey/src/database_connection.dart';
import 'package:mysql1/mysql1.dart';

class DatabaseTester {
  late DatabaseSettings? _dbSettings;
  MySqlConnection? _conn;

  static DatabaseSettings defaultSettings(String databaseName) {
    final envVars = Platform.environment;
    return DatabaseSettings(
      username: 'root',
      password: envVars['MYSQL_ROOT_PASSWORD'] ?? 'secret',
      database: databaseName,
      host: envVars['MYSQL_HOST'] ?? 'localhost',
    );
  }

  Future<void> initialize({
    required DatabaseSettings settings,
    String? tableName,
    String? createSql,
    String? insertSql,
  }) async {
    final connSettings = ConnectionSettings(
      host: settings.host,
      port: settings.port,
      user: settings.username,
      password: settings.password,
    );

    // Ensure database exists (connect without database specified)
    final c = await MySqlConnection.connect(connSettings);
    await c.query(
      'CREATE DATABASE IF NOT EXISTS ${settings.database} CHARACTER SET utf8',
    );
    await c.close();

    // Connect to database
    connSettings.db = settings.database;
    _conn = await MySqlConnection.connect(connSettings);
    _dbSettings = settings;

    // Drop the migrations table
    await runQuery("DROP TABLE IF EXISTS migrations");

    // Create the table, if requested
    if (tableName != null) {
      await _conn!.query('DROP TABLE IF EXISTS $tableName');
      if (createSql != null) {
        await _conn!.query(createSql);
      }
    }

    // Optionally populate the table
    if (insertSql != null) {
      await _conn!.query(insertSql);
    }
  }

  Future<void> cleanup() async {
    if (_conn != null) {
      // Delete the complete test database
      await runQuery("DROP DATABASE IF EXISTS ${_dbSettings!.database}");
      _conn!.close();
    }
  }

  Future<Results> runQuery(String sqlQuery, [List<Object?>? values]) async {
    if (_conn == null) {
      throw Exception(
        "Not connected to test database. Call initialize() first.",
      );
    }
    final result = await _conn!.query(sqlQuery, values);
    return result;
  }

  Future<bool> hasTable(String tableName) async {
    final result = await runQuery("SHOW TABLES LIKE '$tableName'");
    return result.length == 1;
  }
}
