import 'package:dart_monkey/src/database_dev.dart';
import 'package:dart_monkey/src/environment.dart';
import 'package:dart_monkey/src/reflector.dart';
import 'package:dart_monkey/src/resource.dart';
import 'package:test/test.dart';

import 'resource_test.reflectable.dart' as resource_test;

@monkeyReflector
class MockResource extends Resource {
  int? id;
  String name = "";
  int count = 0;

  @override
  String get idFieldName => 'id';

  @override
  String get searchIdFieldName => 'id';
}

void main() {
  const mockTableName = 'mock_resources';
  late DevelopmentDatabase db;

  setUp(() {
    resource_test.initializeReflectable();
    db = DevelopmentDatabase();
    environment = Environment(database: db);
  });

  tearDown(() {
    Environment.reset();
  });

  Future<MockResource> createResourceAndSave() async {
    // Create new resource without id
    final resource = MockResource();
    resource.name = "foo";
    resource.count = 42;
    await resource.save();
    return resource;
  }

  test('has correct row id', () async {
    final resource0 = await createResourceAndSave();
    expect(resource0.id, 0);
    final resource1 = await createResourceAndSave();
    expect(resource1.id, 1);
  });

  test('has correct table name', () {
    final resource = MockResource();
    expect(resource.tableName(), 'mock_resources');
  });

  test('returns expected fields', () async {
    final resource = await createResourceAndSave();
    final fields = resource.fields;

    expect(fields.length, 3);
    expect(fields['id'], resource.id);
    expect(fields['name'], resource.name);
    expect(fields['count'], resource.count);
  });

  test('skips id field on attributes', () async {
    final resource = await createResourceAndSave();
    final attributes = resource.attributes;

    expect(attributes.length, 2);
    expect(attributes['name'], resource.name);
    expect(attributes['count'], resource.count);
  });

  test('skips id field on editable attributes', () async {
    final resource = await createResourceAndSave();
    final attributes = resource.editableAttributes;

    expect(attributes.length, 2);
    expect(attributes['name'], resource.name);
    expect(attributes['count'], resource.count);
  });

  test('saves new resource in database', () async {
    // Create new resource without id
    final resource = await createResourceAndSave();

    // Verify that id has been assigned
    expect(resource.id, 0);
    expect(db.lastChangedFieldsCount, 3); // name, count, id

    // Verify that resource is now part of database
    final dbRows = db.rows(mockTableName);
    expect(dbRows.length, 1);
    expect(dbRows.first, {
      'name': 'foo',
      'count': 42,
      'id': 0,
    });
  });

  test('updates existing resource in database', () async {
    // Create new resource without id
    final resource = await createResourceAndSave();

    // Modify one field and save
    resource.count = 50;
    await resource.save();
    expect(db.lastChangedFieldsCount, 1); // count

    // Verify that database got updated
    final dbRows = db.rows(mockTableName);
    expect(dbRows.length, 1);
    expect(dbRows.first, {
      'name': 'foo',
      'count': 50,
      'id': 0,
    });
  });

  test('succeeds on deleting resource', () async {
    final resource = await createResourceAndSave();
    expect(db.rows(mockTableName).length, 1);

    resource.delete();
    expect(db.rows(mockTableName).length, 0);
  });

  test('fails when trying to delete new resource', () async {
    final resource = MockResource();
    expectLater(resource.delete(), throwsException);
  });
}
