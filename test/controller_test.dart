import 'dart:convert';

import 'package:dart_monkey/dart_monkey.dart';
import 'package:dart_monkey/src/exceptions.dart';
import 'package:test/test.dart';

import 'mock_controller.dart';
import 'mock_validator.dart';

JsonMap testBody() {
  return {
    'data': {
      'type': 'resources',
      'id': 1,
      'attributes': {
        'stringAttr': 'foo string',
        'emptyAttr': '',
      }
    }
  };
}

JsonMap testBodyAttributes() {
  final data = testBody()['data'] as JsonMap;
  return data['attributes'] as JsonMap;
}

Request testRequest(String method) {
  return Request(
    method,
    Uri.http("example.org", "/endpoint"),
    body: jsonEncode(testBody()),
  );
}

class ControllerInput {
  final AttributeMap? attributes;
  final JsonMap? body;
  final Request? request;
  final String? id;

  ControllerInput(this.attributes, this.body, this.request, this.id);
}

void main() {
  void validateInputControllerGot(
    String method,
    ControllerInput recInput,
    Request request,
    String? id,
  ) {
    final sourceHint = "in method $method with id $id";
    expect(recInput.body, testBody(), reason: sourceHint);
    expect(recInput.attributes, testBodyAttributes(), reason: sourceHint);
    expect(recInput.request!.method, method, reason: sourceHint);
    expect(recInput.id, id, reason: sourceHint);
  }

  test('delegates incoming requests to correct handlers', () async {
    final List<Map<String, dynamic>> testCases = [
      {'method': 'POST', 'id': null, 'statusCode': 201},
      {'method': 'GET', 'id': null, 'statusCode': 200},
      {'method': 'GET', 'id': '1', 'statusCode': 202},
      {'method': 'PATCH', 'id': '1', 'statusCode': 203},
      {'method': 'DELETE', 'id': '1', 'statusCode': 204},
    ];

    for (final testCase in testCases) {
      final method = testCase['method'].toString();
      final id = testCase['id'] as String?;
      final statusCode = testCase['statusCode'] as int;

      final request = testRequest(method);

      // Prepare the mock controller to track the input
      ControllerInput? receivedInput;
      final controller = MockController(
          (Controller ctrl, func, method, attributes, body, request, id) async {
        receivedInput = ControllerInput(attributes, body, request, id);
        return ApiResponse(statusCode: statusCode, body: {'method': method});
      });

      final response = await controller.execute(request, id);

      // Verify that controller received the expected input data
      expect(receivedInput, isNotNull, reason: 'on $method($id)');
      validateInputControllerGot(
        method,
        receivedInput!,
        request,
        id,
      );

      // Verify that the controller returned the expected response
      expect(response.statusCode, statusCode);
      expect(jsonDecode(await response.body()), {'method': method});
    }
  });

  test('fails on DELETE without {id}', () async {
    final request = Request('DELETE', Uri.http("example.org", "/endpoint"));
    final controller = MockController();

    final response = await controller.execute(request);
    expect(response.statusCode, 400);
  });

  test('fails on PATCH without {id}', () async {
    final request = Request('PATCH', Uri.http("example.org", "/endpoint"));
    final controller = MockController();

    final response = await controller.execute(request);
    expect(response.statusCode, 400);
  });

  test('fails on unsupported HTTP method', () async {
    final request = Request('HEAD', Uri.http("example.org", "/endpoint"));
    final controller = MockController();

    final response = await controller.execute(request);
    expect(response.statusCode, 405);
  });

  test('reports specific error when validation fails', () async {
    final request = Request('GET', Uri.http("example.org", "/endpoint"));

    // Create a controller which throws a Validation exception
    final controller = MockController(
        (Controller ctrl, func, method, attributes, body, request, id) async {
      throw ValidationException(attrName: 'foo', cause: 'bar');
    });

    final response = await controller.execute(request);
    expect(response.statusCode, 400);
    expect((await response.body()).contains('Validation'), true);
  });

  test('reports specific error when resource not found', () async {
    final request = Request('GET', Uri.http("example.org", "/endpoint"));

    // Create a controller which throws a ResourceNotFound exception
    final controller = MockController(
        (Controller ctrl, func, method, attributes, body, request, id) async {
      throw ResourceNotFoundException(tableName: 'foo_table');
    });

    final response = await controller.execute(request);
    expect(response.statusCode, 404);
    expect((await response.body()).contains('not found'), true);
  });

  test('reports generic error on unknown exceptions', () async {
    final request = Request('GET', Uri.http("example.org", "/endpoint"));

    // Create a controller which throws an exception for all HTTP methods
    final controller = MockController(
        (Controller ctrl, func, method, attributes, body, request, id) async {
      throw Exception('foobar exception');
    });

    final response = await controller.execute(request);
    expect(response.statusCode, 500);
    expect((await response.body()).contains('foobar exception'), true);
  });

  test('validates body', () async {
    final request = Request('GET', Uri.http("example.org", "/endpoint"));

    // Create a controller which causes validation to fail for all HTTP methods
    final failController = MockController(
        (Controller ctrl, func, method, attributes, body, request, id) async {
      ctrl.validate(validators: [MockValidator(fail: true)]);
      return ApiResponse(statusCode: 200);
    });
    expect((await failController.execute(request)).statusCode, 400);

    // Create a controller which causes validation to succeed
    final successController = MockController(
        (Controller ctrl, func, method, attributes, body, request, id) async {
      ctrl.validate(validators: [MockValidator(fail: false)]);
      return ApiResponse(statusCode: 200);
    });
    expect((await successController.execute(request)).statusCode, 200);
  });

  test('passes attributes to handler', () async {
    final request = testRequest('POST');

    // Prepare the mock controller to track the input
    AttributeMap? receivedAttributes;
    final controller = MockController(
        (Controller ctrl, func, method, attributes, body, request, id) async {
      receivedAttributes = attributes;
      return ApiResponse(statusCode: 201);
    });

    final response = await controller.execute(request);
    expect(response.statusCode, 201);
    expect(receivedAttributes, testBodyAttributes());
  });
}
