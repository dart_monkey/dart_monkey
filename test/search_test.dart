import 'package:dart_monkey/main.reflectable.dart';
import 'package:dart_monkey/src/database_dev.dart';
import 'package:dart_monkey/src/database_resource.dart';
import 'package:dart_monkey/src/environment.dart';
import 'package:dart_monkey/src/reflector.dart';
import 'package:dart_monkey/src/search.dart';
import 'package:test/test.dart';

import 'search_test.reflectable.dart' as test_reflector;

@monkeyReflector
class NonExistingResource extends DatabaseResource {}

void main() {
  late DevelopmentDatabase db;
  late DatabaseResource referenceResource0;
  late DatabaseResource referenceResource1;

  setUp(() {
    initializeReflectable();
    test_reflector.initializeReflectable();

    // Start with a database with two rows
    db = DevelopmentDatabase();
    environment = Environment(database: db);
    referenceResource0 = DatabaseResource()..save();
    referenceResource1 = DatabaseResource()..save();
  });

  group('where', () {
    test('finds and returns existing object', () async {
      final resource0 =
          await Search.where<DatabaseResource>('id', referenceResource0.id);
      expect(resource0, isNotNull);
      expect(resource0!.id, referenceResource0.id);
      expect(resource0.createdAt, referenceResource0.createdAt);
      expect(resource0.updatedAt, referenceResource0.updatedAt);

      final resource1 =
          await Search.where<DatabaseResource>('id', referenceResource1.id);
      expect(resource1, isNotNull);
      expect(resource1!.id, 1);
      expect(resource1.createdAt, referenceResource1.createdAt);
      expect(resource1.updatedAt, referenceResource1.updatedAt);
    });

    test('returns null on missing object', () async {
      final resourceWrongField = await Search.where<DatabaseResource>('id', 2);
      expect(resourceWrongField, isNull);

      final resourceWrongType =
          await Search.where<NonExistingResource>('id', 0);
      expect(resourceWrongType, isNull);
    });

    test('converts table and field names to snake case', () async {
      // Initial condition: Verify that the database has table name and createdAt in snake case
      final rawRow = db.rowById('database_resources', referenceResource0.id!);
      expect(rawRow, isNotNull);
      expect(rawRow!.containsKey('created_at'), true);

      // Find that resource and verify that createdAt has been read correctly
      // (although stored in snake case in DB)
      final loadedResource =
          await Search.where<DatabaseResource>('id', referenceResource0.id);
      expect(loadedResource, isNotNull);
      expect(loadedResource!.createdAt, referenceResource0.createdAt);
    });
  });

  group('whereOrFail', () {
    test('finds and returns existing object', () async {
      final resource = await Search.whereOrFail<DatabaseResource>(
        'id',
        referenceResource0.id,
      );
      expect(resource, isNotNull);
      expect(resource.id, 0);
      expect(resource.createdAt, referenceResource0.createdAt);
      expect(resource.updatedAt, referenceResource0.updatedAt);
    });

    test('throws exception on missing object', () async {
      expectLater(
        Search.whereOrFail<DatabaseResource>('id', 2),
        throwsException,
      );
      expectLater(
        Search.whereOrFail<NonExistingResource>('id', 0),
        throwsException,
      );
    });
  });
}
