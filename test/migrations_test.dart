import 'package:dart_monkey/src/database_connection.dart';
import 'package:dart_monkey/src/database_interface.dart';
import 'package:dart_monkey/src/database_sql.dart';
import 'package:dart_monkey/src/migration_executer.dart';
import 'package:dart_monkey/src/server.dart';
import 'package:test/test.dart';

import 'database_tester.dart';
import 'mock_migration.dart';

void main() {
  final dbSettings = DatabaseTester.defaultSettings('migrations_test');
  final dbTester = DatabaseTester();
  late IDatabase database;

  const String firstTableName = 'first_resources';
  const String secondTableName = 'second_resources';

  setUp(() async {
    // Prepare our SQL test database
    await dbTester.initialize(settings: dbSettings);

    // Initialize the SQL database object we want to test
    database = SqlDatabase();
    await database.connect(dbSettings);
  });

  tearDown(() async {
    await database.close();
    await dbTester.cleanup();
  });

  group('migration system', () {
    test('migrates up and down, one migration, one batch', () async {
      final migrations = [
        MockTableMigration('create firstTable', firstTableName),
      ];
      final executer = DatabaseMigrationExecutor(migrations, database);

      // Migrate up creates table
      await executer.up();
      expect(await dbTester.hasTable(firstTableName), true);

      // Migrate down deletes table
      await executer.down();
      expect(await dbTester.hasTable(firstTableName), false);
    });

    test('migrates up and down, two migrations, one batch', () async {
      final migrations = [
        MockTableMigration('create firstTable', firstTableName),
        MockTableMigration('create secondTable', secondTableName),
      ];
      final executer = DatabaseMigrationExecutor(migrations, database);

      // Migrate up creates table
      await executer.up();
      expect(await dbTester.hasTable(firstTableName), true);
      expect(await dbTester.hasTable(secondTableName), true);

      // Migrate down deletes second table
      await executer.down();
      expect(await dbTester.hasTable(firstTableName), true);
      expect(await dbTester.hasTable(secondTableName), false);

      // Migrate down deletes first table
      await executer.down();
      expect(await dbTester.hasTable(firstTableName), false);
      expect(await dbTester.hasTable(secondTableName), false);
    });

    test('migrates up and down, two migrations, two iterations', () async {
      final migrations1 = [
        MockTableMigration('create firstTable', firstTableName),
      ];
      final executer1 = DatabaseMigrationExecutor(migrations1, database);

      final migrations2 = [
        MockTableMigration('create firstTable', firstTableName),
        MockTableMigration('create secondTable', secondTableName),
      ];
      final executer2 = DatabaseMigrationExecutor(migrations2, database);

      // Migrate up: iteration #1
      await executer1.up();
      expect(await dbTester.hasTable(firstTableName), true);
      expect(await dbTester.hasTable(secondTableName), false);

      // Migrate up: iteration #2
      await executer2.up();
      expect(await dbTester.hasTable(firstTableName), true);
      expect(await dbTester.hasTable(secondTableName), true);

      // Migrate down
      await executer2.down();
      expect(await dbTester.hasTable(firstTableName), true);
      expect(await dbTester.hasTable(secondTableName), false);

      // Migrate down again
      await executer1.down();
      expect(await dbTester.hasTable(firstTableName), false);
      expect(await dbTester.hasTable(secondTableName), false);
    });

    test('ignores multiple, identical migrate calls', () async {
      final migrations = [
        MockTableMigration('create firstTable', firstTableName),
      ];
      final executer = DatabaseMigrationExecutor(migrations, database);

      // Run multiple migrate calls. Should ignore the additional ones gracefully
      await executer.up();
      await executer.up();
      await executer.up();
      expect(await dbTester.hasTable(firstTableName), true);

      // Migrate down. Should perform downgrade on first call, ignoring multiple up-calls from above
      await executer.down();
      expect(await dbTester.hasTable(firstTableName), false);
    });

    test('fails on downgrade if database has no migrations', () async {
      final migrations = [MockTableMigration('title', firstTableName)];
      final executer = DatabaseMigrationExecutor(migrations, database);

      expectLater(executer.down(), throwsException);
    });

    test('fails on migrations with same uniqueTitle', () async {
      final migrations = [
        MockTableMigration('same title', firstTableName),
        MockTableMigration('other title', 'other table'),
        MockTableMigration('same title', secondTableName),
      ];
      final executer = DatabaseMigrationExecutor(migrations, database);

      expectLater(executer.up(), throwsException);
    });

    test('fails on migrations with unexpected order', () async {
      // Apply a first migration
      await DatabaseMigrationExecutor(
        [
          MockTableMigration('create firstTable', firstTableName),
        ],
        database,
      ).up();

      // Next try to apply another migration batch. However, in this one
      // our previously first one is not the first one anymore. This is invalid,
      // thus an exception has to be thrown
      expectLater(
        DatabaseMigrationExecutor(
          [
            MockTableMigration('create secondTable', secondTableName),
            MockTableMigration('create firstTable', firstTableName),
          ],
          database,
        ).up(),
        throwsException,
      );
    });

    test('fails on migrations if database contains more migrations than app',
        () async {
      // Apply a first migration
      await DatabaseMigrationExecutor(
        [
          MockTableMigration('create firstTable', firstTableName),
          MockTableMigration('create secondTable', secondTableName),
        ],
        database,
      ).up();

      // Next try to migrate down, but do not provide all currently active migrations
      // Must throw an exception since database and app are out of sync
      expectLater(
        DatabaseMigrationExecutor(
          [
            MockTableMigration('create firstTable', firstTableName),
          ],
          database,
        ).down(),
        throwsException,
      );
    });

    group('integration test', () {
      test('server performs migration', () async {
        final migrations = [
          MockTableMigration('create firstTable', firstTableName),
        ];

        // Verify initial condition: no tables exist
        expect(await dbTester.hasTable('migrations'), false);
        expect(await dbTester.hasTable(firstTableName), false);

        // Run migration via server
        final server = MonkeyServer();
        await server.connectToDatabase(
          conn: DatabaseConnection.mysql,
          settings: dbSettings,
          migrations: migrations,
        );

        // Verify that tables got created
        expect(await dbTester.hasTable('migrations'), true);
        expect(await dbTester.hasTable(firstTableName), true);
      });
    });
  });

  group('migration features', () {
    test('create table', () async {
      // Prepare migrater
      final migrations = [
        MockTableMigration('create firstTable', firstTableName),
      ];
      final executer = DatabaseMigrationExecutor(migrations, database);

      // Verify initial condition
      expect(await dbTester.hasTable(firstTableName), false);

      // Migrate up creates table
      await executer.up();
      expect(await dbTester.hasTable(firstTableName), true);

      // Insert row, to verify it worked
      final upResult = await dbTester.runQuery(
        "INSERT into $firstTableName (name) values (?)",
        ['foo'],
      );
      expect(upResult.insertId, isNotNull);

      // Fetch row, to verify it worked
      final rows = await dbTester.runQuery("SELECT * FROM $firstTableName");
      expect(rows.length, 1);
      expect(rows.first.length, 4);
      expect(rows.first['id'], upResult.insertId);
      expect(rows.first['name'], 'foo');
      expect(rows.first['created_at'], isNotNull);
      expect(rows.first['updated_at'], isNotNull);

      // Migrate down
      await executer.down();

      // Verify that the table got deleted
      expect(await dbTester.hasTable(firstTableName), false);
    });
  });
}
