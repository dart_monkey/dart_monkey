import 'package:dart_monkey/dart_monkey.dart';
import 'package:test/test.dart';

void main() {
  test('constructor with default values', () {
    final settings = DatabaseSettings(
      username: 'user',
      password: 'pwd',
      database: 'db',
    );

    expect(settings.username, 'user');
    expect(settings.password, 'pwd');
    expect(settings.database, 'db');
  });

  test('copyWith', () {
    final settings = DatabaseSettings(
      host: 'host',
      port: 1,
      username: 'user',
      password: 'pwd',
      database: 'db',
    );

    // Copy and replace all values
    final copiedSettings = settings.copyWith(
      host: 'host2',
      port: 2,
      username: 'user2',
      password: 'pwd2',
      database: 'db2',
    );

    expect(copiedSettings.host, 'host2');
    expect(copiedSettings.port, 2);
    expect(copiedSettings.username, 'user2');
    expect(copiedSettings.password, 'pwd2');
    expect(copiedSettings.database, 'db2');

    // Copy and keep all values
    final copiedSettings2 = settings.copyWith();

    expect(copiedSettings2.host, 'host');
    expect(copiedSettings2.port, 1);
    expect(copiedSettings2.username, 'user');
    expect(copiedSettings2.password, 'pwd');
    expect(copiedSettings2.database, 'db');
  });
}
