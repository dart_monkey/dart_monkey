import 'package:dart_monkey/src/controller.dart';
import 'package:dart_monkey/src/response.dart';
import 'package:dart_monkey/src/types.dart';
import 'package:shelf/shelf.dart';

typedef ControllerCallbackFunction = Future<ApiResponse> Function(
  Controller controller,
  String func,
  String httpMethod,
  AttributeMap attributes,
  JsonMap body,
  Request request,
  String? id,
);

/// Mock controller for testing purposes
/// Delegates all incoming requests to the [callbackMethod] passed in the constructor
class MockController extends Controller {
  late final ControllerCallbackFunction _callbackMethod;

  MockController([ControllerCallbackFunction? callback]) {
    if (callback != null) {
      _callbackMethod = callback;
    } else {
      _callbackMethod =
          (controller, func, method, attributes, body, request, id) {
        throw Exception("REST method call but no callback provided in mock");
      };
    }
  }

  @override
  Future<ApiResponse> create(
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) async {
    return _callbackMethod(
      this,
      'create',
      'POST',
      attributes,
      body,
      request,
      null,
    );
  }

  @override
  Future<ApiResponse> delete(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) {
    return _callbackMethod(
      this,
      'delete',
      'DELETE',
      attributes,
      body,
      request,
      id,
    );
  }

  @override
  Future<ApiResponse> read(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) {
    return _callbackMethod(
      this,
      'read',
      'GET',
      attributes,
      body,
      request,
      id,
    );
  }

  @override
  Future<ApiResponse> readAll(
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) {
    return _callbackMethod(
      this,
      'readAll',
      'GET',
      attributes,
      body,
      request,
      null,
    );
  }

  @override
  Future<ApiResponse> update(
    String id,
    AttributeMap attributes,
    JsonMap body,
    Request request,
  ) {
    return _callbackMethod(
      this,
      'update',
      'PATCH',
      attributes,
      body,
      request,
      id,
    );
  }
}
