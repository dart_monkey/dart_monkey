import 'dart:io';

import 'package:dart_monkey/main.reflectable.dart';
import 'package:dart_monkey/src/database_resource.dart';
import 'package:dart_monkey/src/response.dart';
import 'package:dart_monkey/src/types.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'package:test/test.dart';

void main() {
  setUp(() {
    initializeReflectable();
  });

  group('Error response', () {
    test('has correct HTTP status and title', () {
      const JsonMap refJson = {
        'errors': {'status': '400', 'title': 'foo'}
      };
      final response = ApiResponse.error(
        statusCode: HttpStatus.badRequest,
        title: "foo",
      );
      expect(response.json(), completion(equals(refJson)));
    });

    test('only accepts 4xx and 5xx status', () {
      expect(
        () => ApiResponse.error(
          statusCode: HttpStatus.ok,
          title: "foo",
        ),
        throwsException,
      );
    });

    test('has details', () {
      const JsonMap refJson = {
        'errors': {'status': '400', 'title': 'foo', 'detail': 'bar'}
      };
      final response = ApiResponse.error(
        statusCode: HttpStatus.badRequest,
        title: "foo",
        detail: "bar",
      );
      expect(response.json(), completion(equals(refJson)));
    });
  });

  group('Resource response', () {
    DatabaseResource newResource() {
      final resource = DatabaseResource();
      resource.id = 10;
      resource.createdAt = DateTime.now().subtract(const Duration(days: 1));
      resource.updatedAt = DateTime.now();
      return resource;
    }

    shelf.Request newRequest() {
      return shelf.Request(
        'GET',
        Uri.http('example.com', '/endpoint'),
      );
    }

    test('has correct body', () {
      final resource = newResource();
      final request = newRequest();
      final JsonMap refJson = {
        'links': {
          'self': 'http://example.com/endpoint',
        },
        'data': {
          'type': 'database_resources',
          'id': 10,
          'attributes': {
            'created_at': resource.createdAt.toString(),
            'updated_at': resource.updatedAt.toString(),
          }
        }
      };
      final response = ApiResponse.resource(
        statusCode: HttpStatus.ok,
        resource: resource,
        request: request,
      );
      expect(response.json(), completion(equals(refJson)));
    });

    test('has correct HTTP status', () {
      final resource = newResource();
      final request = newRequest();

      final response = ApiResponse.resource(
        statusCode: HttpStatus.created,
        resource: resource,
        request: request,
      );

      expect(response.statusCode, HttpStatus.created);
    });

    test('only accepts 2xx', () {
      final resource = newResource();
      final request = newRequest();

      expect(
        () => ApiResponse.resource(
          statusCode: HttpStatus.badRequest,
          resource: resource,
          request: request,
        ),
        throwsException,
      );
    });
  });
}
