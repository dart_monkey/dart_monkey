import 'package:dart_monkey/src/exceptions.dart';
import 'package:dart_monkey/src/types.dart';
import 'package:dart_monkey/src/validator.dart';

class MockValidator implements IValidator {
  final bool fail;

  MockValidator({required this.fail});

  @override
  void validate(JsonMap requestBody) {
    if (fail) {
      throw ValidationException(attrName: "attr", cause: "mock failed");
    }
  }
}
