import 'package:dart_monkey/dart_monkey.dart';
import 'package:dart_monkey/src/exceptions.dart';
import 'package:test/test.dart';

import 'mock_validator.dart';

class AlwaysFailValidator extends MockValidator {
  AlwaysFailValidator() : super(fail: true);
}

class AlwaysSucceedsValidator extends MockValidator {
  AlwaysSucceedsValidator() : super(fail: false);
}

void main() {
  JsonMap dataJson() {
    return {
      'data': {
        'type': 'resources',
        'id': 1,
        'attributes': {
          'stringAttr': 'foo string',
          'emptyAttr': '',
        }
      }
    };
  }

  group('Group validator', () {
    test('throws if single fails', () {
      final validator = GroupValidator([AlwaysFailValidator()]);
      expect(() => validator.validate(dataJson()), throwsException);
    });

    test('succeeds when single is fine', () {
      final validator = GroupValidator([AlwaysSucceedsValidator()]);
      expect(() => validator.validate(dataJson()), isNot(throwsException));
    });

    test('fails if one of many fails', () {
      final validator = GroupValidator([
        AlwaysSucceedsValidator(),
        AlwaysFailValidator(),
      ]);
      expect(() => validator.validate(dataJson()), throwsException);
    });

    test('succeeds if none of many fails', () {
      final validator = GroupValidator([
        AlwaysSucceedsValidator(),
        AlwaysSucceedsValidator(),
      ]);
      expect(() => validator.validate(dataJson()), isNot(throwsException));
    });
  });

  group('String validator', () {
    group('requires', () {
      test('fails when member is missing', () {
        expect(
          () => StringValidator(path: 'data.missingMember', required: true)
              .validate(dataJson()),
          throwsA(
            predicate(
              (e) =>
                  e is ValidationException &&
                  e.attrName == 'data.missingMember' &&
                  e.cause == 'is required',
            ),
          ),
        );
      });

      test('fails when member is empty', () {
        expect(
          () => StringValidator(path: 'emptyAttr', required: true)
              .validate(dataJson()),
          throwsA(
            predicate(
              (e) =>
                  e is ValidationException &&
                  e.attrName == 'data.attributes.emptyAttr' &&
                  e.cause == 'is required',
            ),
          ),
        );
      });

      test('succeeds on existing attribute', () {
        expect(
          () => StringValidator(path: 'stringAttr', required: true)
              .validate(dataJson()),
          isNot(throwsException),
        );
      });

      test('succeeds on existing path', () {
        expect(
          () => StringValidator(path: 'data.type', required: true)
              .validate(dataJson()),
          isNot(throwsException),
        );
      });
    });
  });
}
