import 'package:dart_monkey/dart_monkey.dart';
import 'package:dart_monkey/src/database_interface.dart';
import 'package:dart_monkey/src/database_sql.dart';
import 'package:test/test.dart';

import 'database_sql_test.reflectable.dart' as test_reflectable;
import 'database_tester.dart';
import 'mock_migration.dart';

DatabaseRow get referenceData {
  return {
    'int_prop': 25,
    'double_prop': 3.5,
    'text_prop': "foo bar",
    'time_prop_utc': DateTime.parse("2022-08-06 14:13:15").toUtc(),
    'time_prop_local': DateTime.parse("2022-08-06 14:13:15").toLocal(),
  };
}

const tableName = 'mock_resources';

const sqlQueryTable = '''
      CREATE TABLE $tableName (
      id INTEGER NOT NULL AUTO_INCREMENT,      
      int_prop INTEGER NOT NULL,
      double_prop DOUBLE NOT NULL,
      text_prop VARCHAR(255) NOT NULL,
      time_prop_utc TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      time_prop_local TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (id)
    );''';

void main() {
  final dbSettings = DatabaseTester.defaultSettings('database_sql_test');
  final dbTester = DatabaseTester();
  late IDatabase database;

  setUp(() async {
    test_reflectable.initializeReflectable();

    // Prepare our SQL test database
    await dbTester.initialize(
      settings: dbSettings,
      tableName: tableName,
      createSql: sqlQueryTable,
    );

    // Initialize the SQL database object we want to test
    database = SqlDatabase();
    await database.connect(dbSettings);
  });

  tearDown(() async {
    await database.close();
    await dbTester.cleanup();
  });

  group('rows', () {
    test('creates new row', () async {
      // Create one new row
      final refData = referenceData;
      final id = await database.createRow(tableName, refData);

      // Verify its content via dbTester
      final returnedData = await dbTester.runQuery("SELECT * FROM $tableName");
      expect(returnedData.length, 1);
      expect(returnedData.first['id'], id);
      expect(returnedData.first['int_prop'], refData['int_prop']);
      expect(returnedData.first['double_prop'], refData['double_prop']);
      expect(returnedData.first['text_prop'], refData['text_prop']);
      expect(
        (returnedData.first['time_prop_utc'] as DateTime).toUtc(),
        refData['time_prop_utc'],
      );
      expect(
        (returnedData.first['time_prop_local'] as DateTime).toLocal(),
        refData['time_prop_local'],
      );
    });

    test('updates existing row', () async {
      // Create two new rows
      final refData = referenceData;
      final id1 = await database.createRow(tableName, refData);
      final id2 = await database.createRow(tableName, refData);

      // Update row #1
      await database.updateRow(tableName, id1, {'text_prop': 'changed'});

      // Verify that resource 1 changed in the database
      var rows =
          await dbTester.runQuery("SELECT * FROM $tableName WHERE id=?", [id1]);
      expect(rows.length, 1);
      expect(rows.first['id'], id1);
      expect(rows.first['text_prop'], 'changed');

      // Verify that resource 2 did NOT change
      rows = await dbTester.runQuery(
        "SELECT * FROM $tableName WHERE id=?",
        [id2],
      );
      expect(rows.length, 1);
      expect(rows.first['id'], id2);
      expect(rows.first['text_prop'], refData['text_prop']);
    });

    test('reads existing row', () async {
      // Create one row
      final refData = referenceData;
      final id = await database.createRow(tableName, refData);

      // Read row
      final returnedRow = await database.readRow(tableName, id);

      // Compare
      expect(returnedRow['id'], id);
      expect(returnedRow['int_prop'], refData['int_prop']);
      expect(returnedRow['double_prop'], refData['double_prop']);
      expect(returnedRow['text_prop'], refData['text_prop']);
      expect(
        (returnedRow['time_prop_utc'] as DateTime).toUtc(),
        refData['time_prop_utc'],
      );
      expect(
        (returnedRow['time_prop_local'] as DateTime).toLocal(),
        refData['time_prop_local'],
      );
    });

    test('fails on reading non existing row', () async {
      expectLater(database.readRow(tableName, 5), throwsException);
    });

    test('deletes existing row', () async {
      // Create two rows
      final refData = referenceData;
      await database.createRow(tableName, refData);
      final id2 = await database.createRow(tableName, refData);

      // Verify it exists
      var rows = await dbTester.runQuery("SELECT * FROM $tableName");
      expect(rows.length, 2);

      // Delete the row
      await database.deleteRow(tableName, id2);

      // Verify it got deleted
      rows = await dbTester.runQuery("SELECT * FROM $tableName");
      expect(rows.length, 1);
    });
  });

  group('search', () {
    test('finds row by single field', () async {
      // Create two rows
      final refData = referenceData;
      await database.createRow(tableName, refData);
      final id2 = await database.createRow(tableName, refData);

      // Find existing row
      final foundRow = await database.findRowByField(tableName, 'id', id2);
      expect(foundRow, isNotNull);
      expect(foundRow!['id'], id2);
    });

    test('return nulls when not finding row', () async {
      // Find existing row
      final foundRow = await database.findRowByField(tableName, 'id', 5);
      expect(foundRow, isNull);
    });
  });

  group('migrations', () {
    test('applies and reverts migration', () async {
      const tableName = 'sample_table';
      const migrationTitle = 'foo migration name';

      // Initial condition: no migrations table availe
      expect(await dbTester.hasTable('migrations'), false);
      final allMigrationsBefore = await database.allMigrationTitles();
      expect(allMigrationsBefore.length, 0);

      // Prepare a migration
      final migration = MockTableMigration(migrationTitle, tableName);

      // Apply the migration
      await database.applyMigration(migration);

      // Verify migration table exists
      expect(await dbTester.hasTable('migrations'), true);

      // Verify that the migration got added to the 'migrations' table
      var rows = await dbTester.runQuery("SELECT * FROM migrations");
      expect(rows.length, 1);
      expect(rows.first['title'], migrationTitle);

      final allMigrationsAfterApply = await database.allMigrationTitles();
      expect(allMigrationsAfterApply.length, 1);

      expect((await database.allMigrationTitles()).length, 1);

      // Revert the migration
      await database.revertMigration(migration);

      // Verify that the migration got removed from the 'migrations' table
      rows = await dbTester.runQuery("SELECT * FROM migrations");
      expect(rows.length, 0);

      final allMigrationsAfterRevert = await database.allMigrationTitles();
      expect(allMigrationsAfterRevert.length, 0);

      // Verify migration table still exists
      expect(await dbTester.hasTable('migrations'), true);
    });

    test('fails when applying same migration twice', () async {
      // Up path
      final migration = MockTableMigration('myMigration', 'my_table');
      await database.applyMigration(migration);
      expectLater(database.applyMigration(migration), throwsException);

      // Down path
      await database.revertMigration(migration);
      expectLater(database.revertMigration(migration), throwsException);
    });

    test('removes migration', () {
      // Prepare a database with one migration
    });

    test('gets active migrations', () {});
  });
}
