import 'dart:io';

import 'package:dart_monkey/dart_monkey.dart';
import 'package:dart_monkey/main.reflectable.dart';
import 'package:dart_monkey/src/database_dev.dart';
import 'package:dart_monkey/src/environment.dart';
import 'package:test/test.dart';

void main() {
  late DevelopmentDatabase db;

  setUp(() {
    initializeReflectable();

    db = DevelopmentDatabase();
    environment = Environment(database: db);
  });

  tearDown(() {
    Environment.reset();
  });

  test('initializes members with null', () {
    final resource = DatabaseResource();
    expect(resource.id, isNull);
    expect(resource.createdAt, isNull);
    expect(resource.updatedAt, isNull);
  });

  test('has id, createdAd and updatedAt', () {
    final resource = DatabaseResource();
    resource.id = 1;
    resource.createdAt = DateTime(1978, 8, 22);
    resource.updatedAt = DateTime(2022, 8, 22);

    expect(resource.id, 1);
    expect(resource.createdAt, DateTime(1978, 8, 22));
    expect(resource.updatedAt, DateTime(2022, 8, 22));
  });

  test('overrides id field names', () {
    final resource = DatabaseResource();
    expect(resource.idFieldName, 'id');
    expect(resource.searchIdFieldName, 'id');
  });

  test('returns the expected fields', () async {
    final resource = DatabaseResource();
    final fields = resource.fields;

    expect(fields.length, 3);
    expect(fields['id'], resource.id);
    expect(fields['created_at'], resource.createdAt);
    expect(fields['updated_at'], resource.updatedAt);
  });

  test('skips id from attributes', () async {
    final resource = DatabaseResource();
    final attributes = resource.attributes;

    expect(attributes.length, 2);
    expect(attributes['created_at'], resource.createdAt);
    expect(attributes['updated_at'], resource.updatedAt);
  });

  test('skips id and createdAt from editable attributes', () async {
    final resource = DatabaseResource();
    final editableAttributes = resource.editableAttributes;

    expect(editableAttributes.length, 1);
    expect(editableAttributes['updated_at'], resource.updatedAt);
  });

  test('fails on update if ID has been modified', () async {
    final resource = DatabaseResource();
    await resource.save();

    resource.id = resource.id! + 1;
    expectLater(resource.save(), throwsException);
  });

  test('fails on update if createdAt has been modified', () async {
    final resource = DatabaseResource();
    await resource.save();

    resource.createdAt = resource.createdAt = DateTime.now().add(
      const Duration(days: 1),
    );
    expectLater(resource.save(), throwsException);
  });

  test('sets the createdAt & updatedAt attributes on initial save ', () async {
    // Create new, saved resource
    final resource = DatabaseResource();
    await resource.save();

    // Verify the created/updated have been filled
    expect(resource.createdAt, isNotNull);
    expect(resource.updatedAt, isNotNull);
    expect(resource.id, 0);

    // Verify that all fields are now part of database
    final dbRows = db.rows(resource.tableName());
    expect(dbRows.length, 1);
    expect(dbRows.first, {
      'id': 0,
      'created_at': resource.createdAt,
      'updated_at': resource.updatedAt,
    });
  });

  test('updates the updatedAt attribute on re-save ', () async {
    // Create new, saved resource
    final resource = DatabaseResource();
    await resource.save();

    final initialUpdatedAt = resource.updatedAt;
    final initialCreatedAt = resource.createdAt;

    // Wait a bit so that our updatedAt field has a chance to change
    sleep(const Duration(milliseconds: 1));

    // Save again
    await resource.save();
    expect(db.lastChangedFieldsCount, 1); // updatedAt

    // Verify that the updateAt date changed, but the createdAt one not
    expect(resource.updatedAt, isNot(initialUpdatedAt));
    expect(resource.createdAt, initialCreatedAt);

    // Verify that database got updated
    final dbRows = db.rows(resource.tableName());
    expect(dbRows.length, 1);
    expect(dbRows.first, {
      'id': 0,
      'created_at': resource.createdAt,
      'updated_at': resource.updatedAt,
    });
  });
}
