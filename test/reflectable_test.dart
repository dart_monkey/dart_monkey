import 'package:dart_monkey/src/reflectable.dart';
import 'package:dart_monkey/src/reflector.dart';
import 'package:test/test.dart';

import 'reflectable_test.reflectable.dart' as test_reflectable;

@monkeyReflector
class ReflectableObject with Reflectable {
  int single = 0;
  int intMember = 42;
  String stringMember = "";
  DateTime? dtMember;
}

void main() {
  setUp(() {
    test_reflectable.initializeReflectable();
  });

  test('supports creating instances', () {
    final instance = Reflectable.createInstance<ReflectableObject>();
    expect(instance, isNotNull);
    expect(instance.intMember, 42);
  });

  test('has capabilities', () {
    // Only needed to get code coverage, since in reality statically created and thus not tracked
    // ignore: prefer_final_locals, prefer_const_constructors
    var reflector = MonkeyReflector();
    expect(reflector.capabilities.length, greaterThan(0));
  });

  test('maps member to field names', () {
    // CamelCase of class members gets snake_case in returned fields
    final instance = ReflectableObject();
    final fields = instance.reflectedFields();
    expect(fields.containsKey('int_member'), true);
    expect(fields.containsKey('string_member'), true);
    expect(fields.containsKey('dt_member'), true);
    expect(fields.containsKey('single'), true);
  });

  test('gets table name in snake_case', () {
    final instance = ReflectableObject();

    // For class instance
    expect(instance.tableName(), 'reflectable_objects');

    // For class type
    expect(
      Reflectable.tableNameForClass(ReflectableObject),
      'reflectable_objects',
    );
  });

  test('exposes fields and their values', () {
    final instance = ReflectableObject();
    final fields = instance.reflectedFields();
    expect(fields.length, 4);
    expect(fields.containsKey('int_member'), true);
    expect(fields['int_member'], 42);
  });

  test('allows checking available of fields by name', () {
    final instance = ReflectableObject();
    expect(instance.hasField('int_member'), true);
    expect(instance.hasField('intMember'), false);
    expect(instance.hasField('missing'), false);
  });

  test('sets single field value', () {
    final instance = ReflectableObject();
    expect(instance.stringMember, "");

    instance.setFieldValue('string_member', 'foo');
    expect(instance.stringMember, 'foo');

    // Verify that setting by variable name (CamelCase) fails
    expect(() => instance.setFieldValue('stringMember', 'x'), throwsException);
  });

  test('sets multiple field values', () {
    final instance = ReflectableObject();

    instance.setFieldValues({
      'string_member': 'foo',
      'dt_member': DateTime(2022, 08, 22),
    });
    expect(instance.stringMember, 'foo');
    expect(instance.dtMember, DateTime(2022, 08, 22));
  });

  test('gets field value', () {
    final instance = ReflectableObject();
    expect(instance.fieldValue('int_member'), 42);
    instance.intMember = 27;
    expect(instance.fieldValue('int_member'), 27);
  });

  test('fails on setting value for non existing field', () {
    final instance = ReflectableObject();
    expect(() => instance.setFieldValue('missing_field', 20), throwsException);
  });

  test('supports checking existence of fields', () {
    final instance = ReflectableObject();
    expect(instance.hasField('int_member'), true);
    expect(instance.hasField('string_member'), true);
    expect(instance.hasField('dt_member'), true);

    expect(instance.hasField('missing_field'), false);
  }); // with id
}
