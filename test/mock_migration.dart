import 'package:dart_monkey/src/migration.dart';

class MockTableMigration extends DatabaseMigration {
  MockTableMigration(this.uniqueTitle, this.tableName);

  final String tableName;

  @override
  final String uniqueTitle;

  @override
  MigrationBlueprint up() {
    return MigrationBlueprint()
      ..createTable(
        '''
        CREATE TABLE IF NOT EXISTS $tableName (
          id INTEGER NOT NULL AUTO_INCREMENT,
          name VARCHAR(255) NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (id)
        );
        ''',
      );
  }

  @override
  MigrationBlueprint down() {
    return MigrationBlueprint()..dropTable(tableName);
  }
}
