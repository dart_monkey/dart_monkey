import 'package:dart_monkey/src/database_connection.dart';
import 'package:dart_monkey/src/database_dev.dart';
import 'package:test/test.dart';

import 'mock_migration.dart';

void main() {
  const tableName = 'test_resources';
  late DevelopmentDatabase database;

  setUp(() async {
    database = DevelopmentDatabase();
    await database.connect(
      DatabaseSettings(database: 'dbname', username: 'user', password: 'pwd'),
    );
  });

  tearDown(() async {
    await database.close();
  });

  group('rows', () {
    test('creates new row', () async {
      await database.createRow(tableName, {'title': 'foo'});

      expect(database.rows(tableName).length, 1);
      expect(database.rowById(tableName, 0), {'id': 0, 'title': 'foo'});
    });

    test('updates existing row', () async {
      await database.createRow(tableName, {'title': 'foo'});
      expect(database.rowById(tableName, 0), {'id': 0, 'title': 'foo'});

      await database.updateRow(tableName, 0, {'title': 'bar'});
      expect(database.rowById(tableName, 0), {'id': 0, 'title': 'bar'});
    });

    test('reads existing row', () async {
      await database.createRow(tableName, {'title': 'foo'});

      final row = await database.readRow(tableName, 0);
      expect(row, {'id': 0, 'title': 'foo'});
    });

    test('deletes existing row', () async {
      await database.createRow(tableName, {'title': 'foo'});
      expect(database.rows(tableName).length, 1);

      await database.deleteRow(tableName, 0);
      expect(database.rows(tableName).length, 0);
    });

    test('finds row by single field', () async {
      await database.createRow(tableName, {'title': 'foo'});
      expect(database.rows(tableName).length, 1);

      final foundRow = await database.findRowByField(tableName, 'id', 0);
      expect(foundRow, {'id': 0, 'title': 'foo'});

      final missingRow = await database.findRowByField(tableName, 'id', 1);
      expect(missingRow, isNull);
    });

    test('supports multiple rows', () async {
      await database.createRow(tableName, {'title': 'foo'});
      await database.createRow(tableName, {'title': 'bar'});

      expect(database.rows(tableName).length, 2);
      expect(database.rowById(tableName, 0), {'id': 0, 'title': 'foo'});
      expect(database.rowById(tableName, 1), {'id': 1, 'title': 'bar'});
    });

    test('separates tables', () async {
      await database.createRow('table_1', {'title': 'foo'});
      await database.createRow('table_2', {'title': 'bar'});

      expect(database.rows('table_1'), [
        {'id': 0, 'title': 'foo'}
      ]);
      expect(database.rows('table_2'), [
        {'id': 0, 'title': 'bar'}
      ]);
    });
  });

  group('migrations', () {
    test('applys and reverts migration', () async {
      const tableName = 'sample_table';
      const migrationTitle = 'foo migration name';
      final migration = MockTableMigration(migrationTitle, tableName);

      // Simualte migration to get code coverage
      await database.applyMigration(migration);
      await database.revertMigration(migration);
      await database.allMigrationTitles();
    });
  });
}
