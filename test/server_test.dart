import 'dart:convert';

import 'package:dart_monkey/dart_monkey.dart';
import 'package:http/http.dart' as http;
import 'package:test/test.dart';

import 'database_tester.dart';
import 'mock_controller.dart';

void main() {
  final dbSettings = DatabaseTester.defaultSettings('server_test');
  late MonkeyServer server;

  /// Controller which echos the passed function, id and body
  final echoController = MockController(
      (Controller c, func, method, attributes, body, request, id) async {
    return ApiResponse(
      statusCode: 200,
      body: {'function': func, 'id': id, 'body': body},
    );
  });

  setUp(() async {
    server = MonkeyServer();
    await server.start(
      address: 'localhost',
      port: 8080,
    );
  });

  tearDown(() async {
    await server.stop();
  });

  test('provides version', () {
    expect(server.version(), isNotEmpty);
  });

  test('connects to database', () async {
    await server.stop();

    try {
      await server.connectToDatabase(
        conn: DatabaseConnection.mysql,
        settings: dbSettings,
      );
    } catch (e) {
      fail("initializing database failed. Details: ${e.toString()}");
    }
  });

  test('fails when connecting to database when server is running', () {
    expectLater(
      server.connectToDatabase(
        conn: DatabaseConnection.mysql,
        settings: dbSettings,
      ),
      throwsException,
    );
  });

  test('routes POST request controller.create', () async {
    server.route('/test', echoController);

    final body = {'body param': 'body value'};
    final response = await http.post(
      Uri.http('localhost:8080', '/test'),
      body: jsonEncode(body),
    );

    final responseBody = jsonDecode(response.body) as JsonMap;
    expect(responseBody, {'function': 'create', 'id': null, 'body': body});
  });

  test('routes GET request without {id} controller.readAll', () async {
    server.route('/test', echoController);

    final response = await http.get(Uri.http('localhost:8080', '/test'));
    final responseBody = jsonDecode(response.body) as JsonMap;
    expect(responseBody, {'function': 'readAll', 'id': null, 'body': {}});
  });

  test('routes GET request with {id} controller.read', () async {
    server.route('/test', echoController);

    final response = await http.get(Uri.http('localhost:8080', '/test/42'));
    final responseBody = jsonDecode(response.body) as JsonMap;
    expect(responseBody, {'function': 'read', 'id': '42', 'body': {}});
  });

  test('routes DELETE request without {id} controller.delete', () async {
    server.route('/test', echoController);

    final response = await http.delete(Uri.http('localhost:8080', '/test/42'));
    final responseBody = jsonDecode(response.body) as JsonMap;
    expect(responseBody, {'function': 'delete', 'id': '42', 'body': {}});
  });

  test('routes PATCH request with {id} controller.update', () async {
    server.route('/test', echoController);

    final body = {'body param': 'body value'};
    final response = await http.patch(
      Uri.http('localhost:8080', '/test/42'),
      body: jsonEncode(body),
    );

    final responseBody = jsonDecode(response.body) as JsonMap;
    expect(responseBody, {'function': 'update', 'id': '42', 'body': body});
  });

  test('returns error for route with missing id', () async {
    server.route('/test', echoController);
    final response = await http.patch(Uri.http('localhost:8080', '/test'));
    expect(response.statusCode, 404);
  });

  test('returns error for unknown route', () async {
    final response = await http.get(Uri.http('localhost:8080', '/test'));
    expect(response.statusCode, 404);
  });

  test('returns error for non-CRUD method', () async {
    final response = await http.head(Uri.http('localhost:8080', '/test'));
    expect(response.statusCode, 404);
  });
}
