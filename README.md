# dart_monkey

[![codecov](https://codecov.io/gl/dart_monkey/dart_monkey/branch/main/graph/badge.svg?token=4URZK61TQF)](https://codecov.io/gl/dart_monkey/dart_monkey)

## Build
- Run: dart run build_runner buil

## Local Coverage
Start "run_coverage.bat" in the terminal to compute the coverage info locally
View it in the Flutter coverage view then.